package com.techtalk.graphqlexample.model;

public class Product {

    private final Integer id;
    private final String name;
    private final String description;
    private final Float price;
    private final Boolean inStock;

    public Product(Integer id, String name, String description, Float price, Boolean inStock) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.inStock = inStock;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Float getPrice() {
        return price;
    }

    public Boolean getInStock() {
        return inStock;
    }

    // You can add additional methods like setters for mutable fields if needed.

    @Override
    public String toString() {
        return "Product{" +
               "id=" + id +
               ", name='" + name + '\'' +
               ", description='" + description + '\'' +
               ", price=" + price +
               ", inStock=" + inStock +
               '}';
    }
}
