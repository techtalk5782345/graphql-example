package com.techtalk.graphqlexample.service;

import com.techtalk.graphqlexample.model.Product;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    public Product findProduct(String id) {
        return new Product(1, "iPhone", "5G", 77000F, true);
    }
}
