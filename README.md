# GraphQL Hello World Project with Spring Boot

## Overview
This project is a simple example of a GraphQL API implemented using Spring Boot. It includes a basic GraphQL schema, query, and a sample service for fetching product data.


## Prerequisites
Make sure you have the following installed:
- Java JDK (version 8 or higher)
- Maven
- Your preferred IDE (e.g., IntelliJ, Eclipse)

## Getting Started
1. Clone this repository to your local machine.
   ```bash
   git clone https://github.com/your-username/graphql-hello-world.git

2. Navigate to graphql-example :

   ```
   cd graphql-example
   ```
3. Build the project:

   ```
   mvn clean install
   ```

4. Run the Product Spring Boot application:

   ```
   mvn spring-boot:run
   ```

5. The Service will be accessible at `http://localhost:8080`

6. After the service is up. You can hit the below API in order to test it.

   ```
   API : http://localhost:8080/getProductById
   
   HTTP Method : POST
   
   Request Body 
   
   {
    findProduct(id:"1"){
      name
      description
      price
    }
   }
   ```